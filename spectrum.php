<?php

/*
//This class converts any visible light wavelengths into RGB
//taking in account visibility curve, max intensity and gamma
//Using an algorithm by Dan Bruton
//
//(c) Daniel Tereschenko, 2012
// http://www.tereschenko.info
//
// 
*/

class Spectrum
{
	const IR = 780; //infrared limit
	const UF = 380; //ultraviolet limit
	const INTENSITY = 255; // #ff in channel at max
	const GAMMA = .8;      // default gamma
	
	protected $intensity;
	protected $gamma;
	
	function __construct($i = self::INTENSITY, $g = self::GAMMA)
	{
		$this->intensity = $i;
		$this->gamma     = $g;
	}

	
	protected function Adjust($color, $factor)
	{
		if (!$color) return 0;
		else return (int)($this->intensity * pow($color * $factor, $this->gamma));
	}
	
	public function getRGB($wave)
	{
		if (($wave >= 380) and ($wave <= 439)) {
			$red   = -($wave - 440) / (440 - 380);
			$green = 0.0;
			$blue  = 1.0;
		}
		elseif ($wave >=440 and $wave <=489) {
			$red   = 0.0;
			$green = ($wave - 440) / (490 - 440);
			$blue  = 1.0;
		}
		elseif ($wave >=490 and $wave <=509) {
			$red   = 0.0;
			$green = 1.0;
			$blue  = -($wave - 510) / (510 - 490);
		}
		elseif ($wave >=510 and $wave <= 579) {
			$red   = ($wave - 510) / (580 - 510);
			$green = 1.0;
			$blue  = 0.0;
		}
		elseif ($wave >= 580 and $wave <= 644) {
			$red   = 1.0;
			$green = -($wave - 645) / (645 - 580);
			$blue  = 0.0;
		}
		elseif ($wave >= 645 and $wave <= 780) {
			$red   = 1.0;
			$green = 0.0;
			$blue  = 0.0;
		}
		else {
			$red   = 0.0;
			$green = 0.0;
			$blue  = 0.0;
		}
		// Let the intensity fall off near the vision limits

		$factor = 0;
		if ($wave >= 380 and $wave <= 419) $factor = 0.3 + 0.7*($wave - 380) / (420 - 380);
		if ($wave >= 420 and $wave <= 700) $factor = 1.0;
		if ($wave >= 701 and $wave <= 780) $factor = 0.3 + 0.7*(780 - $wave) / (780 - 700);

		$red   = $this->Adjust($red,   $factor);
		$green = $this->Adjust($green, $factor);
		$blue  = $this->Adjust($blue,  $factor);

		//add leading zero if needed
		$red   = ($red   >= 16) ? dechex($red)   : '0'.dechex($red);
		$green = ($green >= 16) ? dechex($green) : '0'.dechex($green);
		$blue  = ($blue  >= 16) ? dechex($blue)  : '0'.dechex($blue);
		return $red.$green.$blue;
	}
}
